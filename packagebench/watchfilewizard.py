#!/usr/bin/python3
from PyQt4 import QtGui
from .ui.watchfilewizard import Ui_Wizard
from .watchfile_makers import Github, GoogleCode, Bitbucket, PyPI, HTMLPage, FTP

MAKER_CLASSES = [Github, GoogleCode, Bitbucket, PyPI, HTMLPage, FTP]
makers = {c.display_name: c for c in MAKER_CLASSES}

class WatchFileWizard(QtGui.QWizard):
    
    def __init__(self):
        super().__init__()
        self.ui = Ui_Wizard()
        self.ui.setupUi(self)
        for maker in MAKER_CLASSES:
            self.ui.site_type.addItem(maker.display_name)
        
        self.currentIdChanged.connect(self.on_page_change)
        self.accepted.connect(self.finish)
    
    def on_page_change(self, pageid):
        if pageid == 1:
            self.maker = makers[self.ui.site_type.currentText()](self)
            self.maker.setup_fields()
        elif pageid == 2:
            watchfile = self.maker.generate_watch_file()
            self.ui.watchfile_edit.setPlainText(watchfile)
            
    def finish(self):
        print(self.ui.watchfile_edit.toPlainText())

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    wizard = WatchFileWizard()
    wizard.show()
    app.exec_()
