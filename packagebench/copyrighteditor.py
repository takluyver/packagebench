import os, sys
from PyQt4 import QtGui

from .ui.copyrighteditor import Ui_MainWindow

KNOWN_LICENSES = \
['public-domain', 'Apache-1.0', 'Apache-2.0', 'Artistic-1.0', 'Artistic-2.0',
'BSD-2-clause', 'BSD-3-clause', 'BSD-4-clause', 'ISC', 'CC-BY-1.0', 'CC-BY-2.0',
'CC-BY-2.5', 'CC-BY-3.0', 'CC-BY-SA-1.0', 'CC-BY-SA-2.0', 'CC-BY-SA-2.5',
'CC-BY-SA-3.0', 'CC-BY-ND-1.0', 'CC-BY-ND-2.0', 'CC-BY-ND-2.5', 'CC-BY-ND-3.0',
'CC-BY-NC-1.0', 'CC-BY-NC-2.0', 'CC-BY-NC-2.5', 'CC-BY-NC-3.0',
'CC-BY-NC-SA-1.0', 'CC-BY-NC-SA-2.0', 'CC-BY-NC-SA-2.5', 'CC-BY-NC-SA-3.0',
'CCC-BY-NC-ND-1.0', 'CCC-BY-NC-ND-2.0', 'CCC-BY-NC-ND-2.5', 'CCC-BY-NC-ND-3.0',
'CC0', 'CDDL-1.0', 'CDDL-1.1', 'CPL', 'EFL-1.0', 'EFL-2.0', 'Expat', 'GPL-1.0+',
'GPL-1.0', 'GPL-2.0+', 'GPL-2.0', 'GPL-3.0+', 'GPL-3.0', 'LGPL-2.0+',
'LGPL-2.0', 'LGPL-2.1+', 'LGPL-2.1', 'LGPL-3.0+', 'LGPL-3.0', 'GFDL-1.1',
'GFDL-1.2', 'GFDL-1.3', 'GFDL-NIV-1.1', 'GFDL-NIV-1.2', 'GFDL-NIV-1.3',
'LPPL-1.0', 'LPPL-1.1', 'LPPL-1.2', 'LPPL-1.3c', 'MPL-1.1', 'Perl',
'Python-2.0', 'QPL-1.0', 'W3C', 'Zlib', 'Zope-1.1', 'Zope-2.0', 'Zope-2.1']


class FilegrpItem(QtGui.QStandardItem):
    
    def __init__(self, pattern, copyright, license_name, license_text=""):
        super().__init__()
        self.pattern
        self.copyright = copyright
        self.license_name = license_name
        self.license_text = license_text
    
    def data(self, role=QtCore.Qt.UserRole+1):
        if role == QtCore.Qt.DisplayRole:
            return "{} - {}".format(self.pattern, self.license_name)
        return super().data(role)

class Main(QtGui.QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.filegrps_model = QtGui.QStandardItemModel()
        self.ui.file_groups.setModel(self.filegrps_model)
        self.ui.file_groups.clicked.connect(self.select_filegrp)
    
    def select_filegrp(self, index):
        filegrp_item = self.filegrps_model.itemFromIndex(index)
        self.active_filegrp = filegrp_item
        # Update fields
        self.ui.file_pattern = filegrp_item.pattern
        self.ui.license_name = filegrp_item.license_name
        self.ui.license_text = filegrp_item.license_text
        self.ui.copyright    = filegrp_item.copyright
        
    def new_file_group(self):
        new_filegrp = FilegrpItem("", "", "")
        self.filegrp_model.appendRow(new_filegrp)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())
