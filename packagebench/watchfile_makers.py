from PyQt4 import QtGui

class WatchFileMaker(object):
    display_name = ""
    
    def __init__(self, wizard):
        self.wizard = wizard
        wizard.ui.details_page.setTitle(self.display_name)
        self.fields = {}
    
    def setup_fields(self):
        """Overridden by subclasses."""
        pass
    
    def add_field(self, name, label, content=""):
        ui = self.wizard.ui
        row = len(self.fields)
        lab = QtGui.QLabel(ui.details_page)
        lab.setObjectName(name+"_label")
        lab.setText(label)
        ui.details_form_layout.setWidget(row, QtGui.QFormLayout.LabelRole, lab)
        lineEdit = QtGui.QLineEdit(ui.details_page)
        lineEdit.setObjectName(name)
        lineEdit.setText(content)
        ui.details_form_layout.setWidget(row, QtGui.QFormLayout.FieldRole, lineEdit)
        self.fields[name] = (lab, lineEdit)
    
    def get_field(self, name):
        return self.fields[name][1].text()
    
    def get_fields(self, *names):
        return [self.get_field(n) for n in names]

V3 = "version=3\n"
        
class Github(WatchFileMaker):
    display_name = "Github"
    def setup_fields(self):
        self.add_field("owner", "Username/organisation")
        self.add_field("repo", "Repository")
        self.add_field("tag_re", "Tag regex", "(\d[\d\.]+)")
    
    template = V3+"https://github.com/{}/{}/tags .*/{}\.tar\.gz"
    def generate_watch_file(self):
        values = self.get_fields('owner', 'repo', 'tag_re')
        return self.template.format(*values)

class Bitbucket(WatchFileMaker):
    display_name = "Bitbucket"
    def setup_fields(self):
        self.add_field("owner", "Owner")
        self.add_field("repo", "Repository")
        self.add_field("tag_re", "Tag regex", "(\d[\d\.]+)")
    
    template = V3+"https://bitbucket.org/{}/{}/downloads .*/{}\.tar\.gz"
    def generate_watch_file(self):
        values = self.get_fields('owner', 'repo', 'tag_re')
        return self.template.format(*values)

class GoogleCode(WatchFileMaker):
    display_name = "Google Code"
    def setup_fields(self):
        self.add_field("project_name", "Project name")
    
    template = V3+"http://code.google.com/p/{0}/downloads/list?can=1 .*/{0}-(\d[\d.]*)\.(?:zip|tgz|tbz2|txz|tar\.gz|tar\.bz2|tar\.xz)"
    def generate_watch_file(self):
        project_name = self.get_field('project_name')
        return self.template.format(project_name)

class PyPI(WatchFileMaker):
    display_name = "PyPI"
    def setup_fields(self):
        self.add_field("project_name", "Project name")
        
    template = V3+"http://pypi.python.org/packages/source/{0}/{1}/{1}-(.*)\.tar\.gz"
    
    def generate_watch_file(self):
        project_name = self.get_field('project_name')
        return self.template.format(project_name[0], project_name)
    
class HTMLPage(WatchFileMaker):
    display_name = "Other HTML page"
    def setup_fields(self):
        self.add_field("url", "URL", "http://example.com/releasepage")
        self.add_field("link_re", "Link regex", "downloads/program-(.+)\.tar\.gz")
    
    template = V3+"{} {}"
    def generate_watch_file(self):
        values = self.get_fields("url", "link_re")
        return self.template.format(*values)

class FTP(WatchFileMaker):
    display_name = "FTP/Indexed HTTP"
    def setup_fields(self):
        self.add_field("url", "URL pattern", "ftp://ftp.example.com/pub/program-([\d\.]+)\.tar\.gz")
    
    def generate_watch_file(self):
        return V3 + self.get_field('url')
    
