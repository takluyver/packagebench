import os, sys
from PyQt4 import QtGui

from .ui.mainwindow import Ui_MainWindow

class Main(QtGui.QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.fs_model = QtGui.QFileSystemModel()
        self.ui.treeview_sourcefiles.setModel(self.fs_model)
        self.ui.treeview_sourcefiles.setColumnHidden(2, True)
        self.ui.treeview_sourcefiles.setColumnHidden(3, True)
        self.ui.treeview_sourcefiles.setColumnWidth(0, 200)
        self.ui.treeview_debdir.setModel(self.fs_model)
        self.ui.treeview_debdir.setColumnHidden(2, True)
        self.ui.treeview_debdir.setColumnHidden(3, True)
        self.ui.treeview_debdir.setColumnWidth(0, 200)
        
        #self.set_package_dir('/home/thomas/Code/packaging/notify2/python-notify2-0.1')
    
    def set_package_dir(self, path):
        self.fs_model.setRootPath(path)
        self.ui.treeview_sourcefiles.setRootIndex(self.fs_model.index(path))
        debdir = os.path.join(path, 'debian')
        self.ui.treeview_debdir.setRootIndex(self.fs_model.index(debdir))

def main():
    app = QtGui.QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
