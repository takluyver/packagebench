# coding: utf-8
import csv, textwrap
f = open("license_shortnames.csv")
cr = csv.reader(f)
next(cr)
sns = [r[0] for r in cr]
for l in textwrap.wrap(repr(sns), 80, break_on_hyphens=False):
    print(l)
    
