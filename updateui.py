from PyQt4.uic import compileUi
from glob import glob
import os

def outdated(target, dependencies):
    if not os.path.exists(target):
        return True
    mtime = os.stat(target).st_mtime
    if isinstance(dependencies, str):
        dependencies = [dependencies]
    return any(os.stat(f).st_mtime > mtime for f in dependencies)

for uifile in glob("packagebench/ui/*.ui"):
    pyfile = uifile[:-3] + ".py"
    if outdated(pyfile, uifile):
        print(uifile)
        pyfile = open(pyfile, "wt", encoding="utf-8")
        uifile = open(uifile, "rt", encoding="utf-8")
        compileUi(uifile, pyfile, from_imports=True)
